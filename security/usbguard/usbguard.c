#include <linux/kernel.h>
#include <linux/security.h>
#include <linux/socket.h>
#include <linux/dcache.h>
#include <net/sock.h>
#include <net/netlink.h>
#include <linux/usb.h>
#include <linux/usb/hcd.h>


static int guard_socket_create(int family, int type, int protocol, int kern)
{
	int id, chix;
	struct usb_device *dev, *childdev = NULL;
	struct usb_bus *ubus = NULL;
	int retval = -ENODEV;

	if (kern) {
		return 0;
	}

	switch(family){
	case AF_INET:
		if(protocol == IPPROTO_IP)
			break;
	default:
		return 0;
	}

	mutex_lock(&usb_bus_idr_lock);
	idr_for_each_entry(&usb_bus_idr, ubus, id)
	{
		dev = ubus->root_hub;
		usb_hub_for_each_child(dev, chix, childdev)
		{
			if(childdev)
			{
				printk("YOYO: Vendor Id:%x, Product Id:%x\n"
				       ,childdev->descriptor.idVendor
				       ,childdev->descriptor.idProduct);
				if(childdev->descriptor.idVendor == 0x125f
				   && childdev->descriptor.idProduct == 0xdb8a)
				{
					printk("YOYO: through");
					retval = 0;
					goto out;
				}

			}
		}

	}
out:
	mutex_unlock(&usb_bus_idr_lock);
	return retval;

}
static struct security_operations guard_ops = {
	.name =		"usbguard",
	.socket_create = guard_socket_create
};


int __init lsm_test(void) {

	if (!security_module_enable(&guard_ops))
		return 0;

    printk(KERN_INFO "usb gaurd initializing.\n");

    if (register_security(&guard_ops))
		panic("usbguard: Unable to register with kernel.\n");

    return 0;
}

security_initcall(lsm_test);
