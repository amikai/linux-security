cp arch/x86/boot/bzImage /boot/vmlinuz-3.10.0-862.3.2.USBGUARD

cp .config /boot/config-3.10.0-862.3.2.USBGUARD
chmod a+x /boot/vmlinuz-3.10.0-862.3.2.USBGUARD
cp System.map /boot/System.map-3.10.0-862.3.2.USBGUARD
gzip -c Module.symvers > /boot/symvers-3.10.0-862.3.2.USBGUARD.gz
restorecon -Rv /boot
dracut -f -v /boot/initramfs-3.10.0-862.3.2.USBGUARD.img 3.10.0.USBGUARD
grub2-mkconfig -o /boot/grub2/grub.cfg
